# Introduction à Terraform



## Installation

- Installer Azure CLI: https://learn.microsoft.com/fr-fr/cli/azure/install-azure-cli
- Installer Terraform: https://developer.hashicorp.com/terraform/downloads
- Ajouter l'executable de Terraform dans les variables d'environnement.

## Travail à faire

### Création d'un VM sur Azure

- Cloner le projet 
- Utiliser ``` terraform init ``` pour initaliser le projet.
- Formatter le code avec ``` terraform fmt ```
- Prévisualiser les changements avec ``` terraform plan ```
- Exécuter le plan avec ``` terraform apply ```
- Détruire la ressource avec ``` terraform destroy ```

